<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassStudent.php';
    include_once 'php/model/ClassProfessor.php';

    session_start();

    if(empty($_SESSION['user']))
    {
        header("Location: login.php");
        exit();
    }
    else
    {
        $user = $_SESSION['user'];
        $type = $_SESSION['type'];

        if($type == "student")
        {
            header("Location: index.php");
            exit();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ProjectRev - Create group</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom -->
    <link href="css/fashion.css" rel="stylesheet">
    <!-- Quill -->
    <link rel="stylesheet" href="css/quill.snow.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Main menu -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ProjectRev v1.0</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="groups.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Find group</a></li>
              </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-push-8">
                <p>Welcome, <?php print $user->get_name(); ?> (<a href="php/logic/logout.php">logout</a>)</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Create group</h1>
            </div>

            <div class="col-md-8 col-md-push-2">
                <br><br>
                <form id="form_create_group" name="form_create_group" role="form">
                    <fieldset>
                        <div class="form-group">
                            <div id="alert_name"></div>
                            <input id="input_name" class="form-control" placeholder="Name" name="name" type="text" autofocus>
                        </div>
                        <div class="form-group">
                            <div id="alert_about"></div>
                            <div id="editor-container">
                        </div>
                        </div>
                        
                        <div id="alert_info"></div>
                        <!-- Change this to a button or input when using this as a form -->
                        <button id="create_group_btn" type="submit" class="btn btn-lg btn-primary btn-block">Create</button>
                    </fieldset>
                </form>
            </div>
        </div>
        <br><br><br>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Quill -->
    <script src="js/quill.min.js"></script>

    <script type="text/javascript">
        var quill = new Quill('#editor-container', {
            modules: { toolbar: true },
            placeholder: 'About...',
            theme: 'snow'  // or 'bubble'
        });
    </script>

    <script src="js/custom/create_group.js"></script>
</body>
</html>