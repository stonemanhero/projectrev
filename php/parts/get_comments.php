<?php
	include_once '../database/db.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassProfessor.php';
	include_once '../model/ClassComment.php';

	$comments = Comment::select_all($_POST['project_id']);

	if(!empty($comments))
	{
		foreach ($comments as $key => $value) 
		{
			print '<hr>';
			print '<div>';
				print '<p>Comment by <b>' . $value->get_name() . '</b><i> (' . $value->get_dateandtime() . ')</i></p>';
				print '<p>' . $value->get_message() . '</p>';
			print '</div>';
		}
	}
	else
	{
		print '<br><div class="alert alert-warning">No comments for this project.</div>';
	}