<?php
    include_once '../database/db.php';
    include_once '../model/ClassProjectHasStudent.php';
    // Manage members
    $members = ProjectHasStudent::get_members($_POST['id']);

    print '<div class="row">
                <br><br>
                <div class="col-md-8 col-md-push-2">
                    <p class="text-center">Other members</p>';
    if(!empty($members)) {
        print ' <div id="table_mygroups">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table borderless">
                                <thead>
                                    <tr>
                                        <th><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Student</th>
                                        <th><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> ID</th>
                                        <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                    </tr>
                                </thead>
                                <tbody>';

        foreach ($members as $key => $value) {
            print '<tr>';
                print '<td>' . $value->get_student_name() . '</td>';
                print '<td>' . $value->get_student_index_number() . '</td>';
                print '<td><button data-student_id="' . $value->get_student_id() . '" data-project_id="' . $value->get_project_id() . '" class="btn btn-danger remove-member"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> Remove</button></td>';
            print '</tr>';
        }

        print '</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>';
    }
    else {
        print '<br><div class="alert alert-warning">No other members on this project.</div>';
    }

    print '</div>';