<?php
	include_once '../database/db.php';
	include_once '../model/ClassGroup.php';
	include_once '../model/ClassProfessor.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassGroupHasStudent.php';

	session_start();
	
	$data = array();

	if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
    	if($_SESSION['type'] == "professor")
    	{
    		$group_has_student = new GroupHasStudent();
    		$group_has_student->set_group_id($_POST['group_id']);
    		$group_has_student->set_student_id($_POST['student_id']);
			$group_has_student->set_status($_POST['status']);
			$group_has_student->update();
			$data['info'] = "ok";
    	}
    	else
    	{
    		$data['error'] = "Only professor type of user can request access to group.";
    	}
	}

	print json_encode($data);
?>