<?php
	include_once '../database/db.php';
	include_once '../model/ClassProfessor.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassComment.php';

	session_start();
	
	$data = array();
	$status = 1;

	if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
		if(empty($_POST['name']))
		{
			$status = 0;
			$data['error'] = "Bad name";
		}

		if(empty($_POST['project_id']))
		{
			$status = 0;
			$data['error'] = "Bad project!";
		}

		if(empty($_POST['message']))
		{
			$status = 0;
			$data['error'] = "Comment field can't be empty";
		}

		if($status == 1)
		{
			$comment = new Comment();
			$comment->set_name($_POST['name']);
			$comment->set_project_id($_POST['project_id']);
			$comment->set_message($_POST['message']);
			$comment->insert();

			$data['info'] = "ok";
		}
	}

	print json_encode($data);
?>