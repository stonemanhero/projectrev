<?php
include_once '../database/db.php';
include_once '../model/ClassGroup.php';
include_once '../model/ClassProfessor.php';

session_start();

$data = array();
$status = 1;

if(empty($_SESSION['user']))
{
    $data['error'] = "You are not logged in.";
}
else
{
    if($_SESSION['type'] == "professor")
    {
        if(empty($_POST['name']))
        {
            $status = 0;
            $data['name'] = "This field can't be empty";
        }

        if(empty($_POST['about']))
        {
            $status = 0;
            $data['about'] = "This field can't be empty";
        }

        if(empty($_POST['group_id'])) {
            $status = 0;
            $data['error'] = "Wrong group";
        }

        if($status == 1)
        {
            if(Group::owner($_POST['group_id'], $_SESSION['user']->get_id()))
            {
                $group = new Group();
                $group->set_name($_POST['name']);
                $group->set_about($_POST['about']);
                $group->set_id($_POST['group_id']);
                $group->update();

                $data['info'] = "ok";
            }
            else
            {
                $data['error'] = "You are not owner of this group or group doesn't exist.";
            }
        }
    }
    else
    {
        $data['error'] = "Only professor type of user can create group.";
    }
}

print json_encode($data);
?>