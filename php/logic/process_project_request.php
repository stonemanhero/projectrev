<?php
    include_once '../database/db.php';
    include_once '../model/ClassGroup.php';
    include_once '../model/ClassProfessor.php';
    include_once '../model/ClassStudent.php';
    include_once '../model/ClassGroupHasStudent.php';
    include_once '../model/ClassProject.php';

    session_start();

    $data = array();

    if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
        if($_SESSION['type'] == "professor")
        {
            $project = new Project();
            $project->set_id($_POST['project_id']);
            $project->set_status($_POST['status']);
            $project->update();
            $data['info'] = "ok";
        }
        else
        {
            $data['error'] = "Only professor type of user can request access to group.";
        }
    }

    print json_encode($data);
?>