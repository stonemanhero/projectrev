<?php
	include_once '../database/db.php';
	include_once '../model/ClassGroup.php';
	include_once '../model/ClassProfessor.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassGroupHasStudent.php';

	session_start();
	
	$data = array();

	if(empty($_SESSION['user']))
    {
        $data['error'] = "Please, logout to register new user.";
    }
    else
    {
    	if($_SESSION['type'] == "student")
    	{
			GroupHasStudent::request_group_access($_POST['group_id'], $_POST['professor_id'], $_POST['user_id']);
			$data['info'] = "ok";
    	}
    	else
    	{
    		$data['error'] = "Only student type of user can request access to group.";
    	}
	}

	print json_encode($data);
?>