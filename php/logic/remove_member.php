<?php
	include_once '../database/db.php';
	include_once '../model/ClassGroup.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassGroupHasStudent.php';
	include_once '../model/ClassProjectHasStudent.php';

	session_start();
	
	$data = array();
	$status = 1;

	if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
    	if($_SESSION['type'] == "student")
    	{
			if(empty($_POST['project_id']))
			{
				$status = 0;
				$data['error'] = "Missing project id!";
			}

			if(empty($_POST['student_id']))
			{
				$status = 0;
				$data['error'] = "Missing student id!";
			}

			if($status == 1)
			{

				if(ProjectHasStudent::is_member($_POST['student_id'], $_POST['project_id']))
				{
					$project_has_student = new ProjectHasStudent();
					$project_has_student->set_project_id($_POST['project_id']);
					$project_has_student->set_student_id($_POST['student_id']);
					$project_has_student->remove();

					$data['info'] = "ok";
				}
				else
				{
					$data['error'] = "This student is not member of this project.";
				}
			}
    	}
    	else
    	{
    		$data['error'] = "Only student type of user can manage members.";
    	}
	}

	print json_encode($data);
?>