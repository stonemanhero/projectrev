<?php
	include_once '../database/db.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassProfessor.php';

	session_start();
	
	$data = array();
	$status = 1;

	if(!empty($_SESSION['user']))
    {
        $data['error'] = "Please, logout to register new user.";
    }
    else
    {
		if(empty($_POST['email']))
		{
			$status = 0;
			$data['email'] = "This field can't be empty";
		}
		else
		{
			if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)
			{
				$status = 0;
				$data['email'] = "Bad email format";
			}
		}

		if(empty($_POST['index_number']))
		{
			$status = 0;
			$data['index_number'] = "This field can't be empty";
		}

		if(empty($_POST['name']))
		{
			$status = 0;
			$data['name'] = "This field can't be empty";
		}

		if(empty($_POST['password']))
		{
			$status = 0;
			$data['password'] = "This field can't be empty";
		}

		if(empty($_POST['password2']))
		{
			$status = 0;
		}

		if($status == 1)
		{
			if($_POST['password'] != $_POST['password2'])
			{
				$status = 0;
				$data['password'] = "Passwords don't match";
			}
		}

		if($status == 1)
		{
			if(Student::available($_POST['email']))
			{
				$student = new Student();
				$student->set_email($_POST['email']);
				$student->set_password($_POST['password']);
				$student->set_name($_POST['name']);
				$student->set_index_number($_POST['index_number']);
				$student->register();

				$data['info'] = "ok";
			}
			else
			{
				$data['error'] = "There is already registered student with this email.";
			}
		}
	}

	print json_encode($data);
?>