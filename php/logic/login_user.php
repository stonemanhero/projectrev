<?php
	include_once '../database/db.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassProfessor.php';

	session_start();
	
	$data = array();
	$status = 1;

	if(!empty($_SESSION['user']))
    {
        $data['error'] = "User is already signed in.";
    }
    else
    {
		if(empty($_POST['email']))
		{
			$status = 0;
			$data['email'] = "This field can't be empty";
		}

		if(empty($_POST['password']))
		{
			$status = 0;
			$data['password'] = "This field can't be empty";
		}

		if($status == 1)
		{
			if(($user = Student::login($_POST['email'], $_POST['password'])) != null)
			{
				$_SESSION['user'] = $user;
				$_SESSION['type'] = "student";

				$data['info'] = "ok";
			}
			else if(($user = Professor::login($_POST['email'], $_POST['password'])) != null)
			{
				$_SESSION['user'] = $user;
				$_SESSION['type'] = "professor";

				$data['info'] = "ok";
			}
			else
				$data['error'] = "Authentication failed.";
		}
		else
		{
			$data['error'] = "Authentication failed.";
		}
	}

	print json_encode($data);
?>