<?php
    include_once '../database/db.php';
    include_once '../model/ClassProject.php';
    include_once '../model/ClassStudent.php';

    session_start();

    $data = array();
    $status = 1;

    if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
        if($_SESSION['type'] == "student")
        {
            if(empty($_POST['name']))
            {
                $status = 0;
                $data['name'] = "This field can't be empty";
            }

            if(empty($_POST['project_id'])) {
                $status = 0;
                $data['info'] = "Wrong project";
            }

            if($status == 1)
            {
                if(Project::owner($_POST['project_id'], $_SESSION['user']->get_id()))
                {
                    $project = new Project();
                    $project->set_name($_POST['name']);

                    if(!empty($_POST['description']))
                        $project->set_description($_POST['description']);
                    else
                        $project->set_description(null);

                    if(!empty($_POST['link']))
                        $project->set_link($_POST['link']);
                    else
                        $project->set_link(null);

                    $project->set_id($_POST['project_id']);
                    $project->update_edit();

                    $data['info'] = "ok";
                }
                else
                {
                    $data['error'] = "You are not owner of this project or project doesn't exist.";
                }
            }
        }
        else
        {
            $data['error'] = "Only student type of user can edit project.";
        }
    }

print json_encode($data);