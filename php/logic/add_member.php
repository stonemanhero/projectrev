<?php
	include_once '../database/db.php';
	include_once '../model/ClassGroup.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassProfessor.php';
	include_once '../model/ClassGroupHasStudent.php';
	include_once '../model/ClassProjectHasStudent.php';

	session_start();
	
	$data = array();
	$status = 1;

	if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
    	if($_SESSION['type'] == "student")
    	{
			if(empty($_POST['email']))
			{
				$status = 0;
				$data['error'] = "This field can't be empty";
			}

			if(empty($_POST['project_id']))
			{
				$status = 0;
				$data['error'] = "Bad project!";
			}

			if(empty($_POST['group_id']))
			{
				$status = 0;
				$data['error'] = "Bad group!";
			}		

			if($status == 1)
			{
				if($_POST['email'] != $_SESSION['user']->get_email())
				{
					$new_student_id = Student::find_id($_POST['email']);
					if($new_student_id != -1)
					{
						if(GroupHasStudent::is_member($new_student_id, $_POST['group_id']))
						{
							if(!ProjectHasStudent::is_member($new_student_id, $_POST['project_id']))
							{
								$project_has_student = new ProjectHasStudent();
								$project_has_student->set_project_id($_POST['project_id']);
								$project_has_student->set_project_group_id($_POST['group_id']);
								$project_has_student->set_project_group_professor_id($_POST['group_professor_id']);
								$project_has_student->set_project_student_id($_SESSION['user']->get_id());
								$project_has_student->set_student_id($new_student_id);

								$project_has_student->insert();

								$data['info'] = "ok";
							}
							else
							{
								$data['error'] = "Student with this email is already team member.";
							}
						}
						else
						{
							$data['error'] = "Student with this email is not member of the project's group.";
						}
					}
					else
					{
						$data['error'] = "There is no student with this email.";
					}
				}
				else
				{
					$data['error'] = "You are leader of this group";
				}
			}
    	}
    	else
    	{
    		$data['error'] = "Only student type of user can manage members.";
    	}
	}

	print json_encode($data);
?>