<?php
	include_once '../database/db.php';
	include_once '../model/ClassStudent.php';

	session_start();

	if(!empty($_SESSION['user']))
    {
        unset($_SESSION['user']);
        unset($_SESSION['type']);

        session_regenerate_id();
    }

    header("Location: ../../login.php");
?>