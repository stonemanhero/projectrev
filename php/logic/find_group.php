<?php
	include_once '../database/db.php';
	include_once '../model/ClassGroup.php';
	include_once '../model/ClassProfessor.php';
	include_once '../model/ClassStudent.php';
	include_once '../model/ClassGroupHasStudent.php';

	session_start();
	
	$data = array();

	if(empty($_SESSION['user']))
    {
        $data['error'] = "Please, logout to register new user.";
    }
    else
    {
        $data['info'] = "";
		$groups = Group::find($_POST['search_text']);

        if(!empty($groups))
        {
            $data['info'] = $data['info'] . '<div class="row">
                            <div>
                                <p class="pagealerts text-center"></p>
                            </div>
                            <div id="table_mygroups">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table borderless">
                                            <thead>
                                                <tr>
                                                    <th><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> Group</th>
                                                    <th><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Professor</th>
                                                    <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Creation date</th>
                                                    <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>';
            foreach ($groups as $key => $value) 
            {
                $data['info'] = $data['info'] . '<tr>';
                    $data['info'] = $data['info'] . '<td>' . $value->get_name() . '</td>';
                    $data['info'] = $data['info'] . '<td>' . $value->get_professor_name() . '</td>';
                    $data['info'] = $data['info'] . '<td>' . $value->get_creation_date() . '</td>';
                    $data['info'] = $data['info'] . '<td><a href="group.php?id=' . $value->get_id() . '"><button class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</button></a></td>';
                $data['info'] = $data['info'] . '</tr>';
            }

            $data['info'] = $data['info'] . '                            </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>';
        }
        else
        {
            $data['info'] = '<div class="alert alert-warning"><strong>No results!</strong></div>';
        }
		
	}

	print json_encode($data);
?>