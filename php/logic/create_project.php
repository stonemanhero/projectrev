<?php
	include_once '../database/db.php';
    include_once '../model/ClassGroup.php';
	include_once '../model/ClassProject.php';
	include_once '../model/ClassProfessor.php';
	include_once '../model/ClassStudent.php';

	session_start();
	
	$data = array();
	$status = 1;

	if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
    	if($_SESSION['type'] == "student")
    	{
			if(empty($_POST['name']))
			{
				$status = 0;
				$data['name'] = "This field can't be empty";
			}

			if(empty($_POST['group_id']))
			{
				$status = 0;
				$data['group_id'] = "This field can't be empty";
			}

			if($status == 1)
			{
				if(Project::available($_POST['name'], $_POST['group_id']))
				{
					$professor_id = Group::get_group_owner($_POST['group_id']);
					
					if($professor_id != -1) 
					{
						$project = new Project();
						$project->set_name($_POST['name']);
						$project->set_group_id($_POST['group_id']);
						$project->set_group_professor_id($professor_id);
						$project->set_student_id($_SESSION['user']->get_id());

						if(!empty($_POST['description']))
							$project->set_description($_POST['description']);
						else
							$project->set_description(null);

						if(!empty($_POST['link']))
							$project->set_link($_POST['link']);
						else
							$project->set_link(null);

						$project->insert();

						$data['info'] = "ok";
					}
					else
						$data['error'] = "You shoudn't change hidden parameters on your own!";
				}
				else
				{
					$data['error'] = "There is already project with this name.";
				}
			}
    	}
    	else
    	{
    		$data['error'] = "Only student type of user can create project.";
    	}
	}

	print json_encode($data);
?>