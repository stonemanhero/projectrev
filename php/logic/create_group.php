<?php
	include_once '../database/db.php';
	include_once '../model/ClassGroup.php';
	include_once '../model/ClassProfessor.php';

	session_start();
	
	$data = array();
	$status = 1;

	if(empty($_SESSION['user']))
    {
        $data['error'] = "You are not logged in.";
    }
    else
    {
    	if($_SESSION['type'] == "professor")
    	{
			if(empty($_POST['name']))
			{
				$status = 0;
				$data['name'] = "This field can't be empty";
			}

			if(empty($_POST['about']))
			{
				$status = 0;
				$data['about'] = "This field can't be empty";
			}

			if($status == 1)
			{
				if(Group::available($_POST['name']))
				{
					$group = new Group();
					$group->set_name($_POST['name']);
					$group->set_about($_POST['about']);
					$group->set_professor_id($_SESSION['user']->get_id());
					$group->insert();

					$data['info'] = "ok";
				}
				else
				{
					$data['error'] = "There is already group with this name.";
				}
			}
    	}
    	else
    	{
    		$data['error'] = "Only professor type of user can create group.";
    	}
	}

	print json_encode($data);
?>