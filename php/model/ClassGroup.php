<?php
	class Group
	{
		private $id;
		private $name;
		private $about;
		private $creation_date;
		private $professor_id;
		private $professor_name;
		private $status;

		// setters
		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_name($new_name)
		{
			$this->name = $new_name;
		}

		public function set_about($new_about)
		{
			$this->about = $new_about;
		}

		public function set_creation_date($new_creation_date)
		{
			$this->creation_date = $new_creation_date;
		}

		public function set_professor_id($new_professor_id)
		{
			$this->professor_id = $new_professor_id;
		}

		public function set_professor_name($new_professor_name)
		{
			$this->professor_name = $new_professor_name;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		// getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_name()
		{
			return $this->name;
		}

		public function get_about()
		{
			return $this->about;
		}

		public function get_creation_date()
		{
			return $this->creation_date;
		}

		public function get_professor_id()
		{
			return $this->professor_id;
		}

		public function get_professor_name()
		{
			return $this->professor_name;
		}

		public function get_status()
		{
			return $this->status;
		}

		// Check if group name available
		static public function available($name)
		{
			$db = new DataLayer();
			$db->query("SELECT id FROM `group` WHERE name=:name");
			$db->bind('name', $name);
			$db->execute();

			if($db->rowCount() > 0)
				return false;

			return true;
		}

        // Check if group name available
        static public function owner($group_id, $professor_id)
        {
            $db = new DataLayer();
            $db->query("SELECT id FROM `group` WHERE id=:id AND professor_id=:professor_id");
            $db->bind('id', $group_id);
            $db->bind('professor_id', $professor_id);
            $db->execute();

            if($db->rowCount() > 0)
                return true;

            return false;
        }

		// Get professor id
		static public function get_group_owner($id)
		{
			$db = new DataLayer();
			$db->query("SELECT professor_id FROM `group` WHERE id=:id");
			$db->bind('id', $id);
			$db->execute();

			if($db->rowCount() > 0)
				return $db->single()['professor_id'];

			return -1;
		}

		// Get group info
		static public function select($id)
		{
			$db = new DataLayer();
			$db->query('SELECT `professor`.name AS professor_name, `group`.id, `group`.name, `group`.about, `group`.creation_date,
                              `group`.status, `group`.professor_id FROM `group` JOIN `professor` ON `group`.professor_id = `professor`.id 
                              WHERE `group`.id=:id AND `group`.status=1');
			$db->bind('id', $id);
			$db->execute();

			if($db->rowCount() > 0)
			{
				$group = new Group();
				$group->set_id($db->single()['id']);
				$group->set_name($db->single()['name']);
				$group->set_about($db->single()['about']);
				$group->set_creation_date($db->single()['creation_date']);
				$group->set_professor_id($db->single()['professor_id']);
				$group->set_professor_name($db->single()['professor_name']);
				$group->set_status($db->single()['status']);

				return $group;
			}

			return null;
		}

		// Get all groups by professor
		static public function select_by_professor($id)
		{
			$db = new DataLayer();
			$db->query('SELECT * FROM `group` WHERE professor_id=:id AND status=1');
			$db->bind('id', $id);
			$db->execute();

			$groups_db = array();
			if($db->rowCount() > 0)
			{
				$data = $db->resultset();

				foreach ($data as $key => $value) 
				{
					$group = new Group();
					$group->set_id($value['id']);
					$group->set_name($value['name']);
					$group->set_about($value['about']);
					$group->set_creation_date($value['creation_date']);
					$group->set_professor_id($value['professor_id']);
					$group->set_status($value['status']);

					array_push($groups_db, $group);
				}
			}

			return $groups_db;
		}

		// Find all groups
		static public function find($search_text)
		{
			$search_text = '%' . $search_text . '%';
			$db = new DataLayer();
			$db->query('SELECT `professor`.name AS professor_name, `group`.id, `group`.name, `group`.about, `group`.creation_date, 
                              `group`.status, `group`.professor_id FROM `group` JOIN `professor` ON `group`.professor_id = `professor`.id 
                              WHERE `group`.name LIKE :search_text AND `group`.status=1');
			$db->bind('search_text', $search_text);
			$db->execute();

			$groups_db = array();
			if($db->rowCount() > 0)
			{
				$data = $db->resultset();

				foreach ($data as $key => $value) 
				{
					$group = new Group();
					$group->set_id($value['id']);
					$group->set_name($value['name']);
					$group->set_about($value['about']);
					$group->set_creation_date($value['creation_date']);
					$group->set_professor_id($value['professor_id']);
					$group->set_professor_name($value['professor_name']);
					$group->set_status($value['status']);

					array_push($groups_db, $group);
				}
			}

			return $groups_db;
		}

		// Insert new Group
		public function insert()
		{
			$date_now = new DateTime(null, new DateTimeZone('Europe/Belgrade'));
			$this->set_creation_date($date_now->format('Y-m-d H:i:s'));

			$db = new DataLayer();
			$db->query('INSERT INTO `group` (name, about, creation_date, professor_id) VALUES (:name, :about, :creation_date, :professor_id)');
			$db->bind('name', $this->name);
			$db->bind('about', $this->about);
			$db->bind('creation_date', $this->creation_date);
			$db->bind('professor_id', $this->professor_id);
			$db->execute();
		}

        // Update group
        public function update()
        {
            $db = new DataLayer();
            $db->query('UPDATE `group` SET name=:name, about=:about WHERE id=:id');
            $db->bind('name', $this->name);
            $db->bind('about', $this->about);
            $db->bind('id', $this->id);
            $db->execute();
        }


	}