<?php
	class Comment
	{
		private $id;
		private $message;
		private $dateandtime;
		private $status;
		private $name;
		private $project_id;

		// setters
		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_message($new_message)
		{
			$this->message = $new_message;
		}

		public function set_dateandtime($new_dateandtime)
		{
			$this->dateandtime = $new_dateandtime;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		public function set_name($new_name)
		{
			$this->name = $new_name;
		}

		public function set_project_id($new_project_id)
		{
			$this->project_id = $new_project_id;
		}

		// getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_message()
		{
			return $this->message;
		}

		public function get_dateandtime()
		{
			return $this->dateandtime;
		}

		public function get_status()
		{
			return $this->status;
		}

		public function get_name()
		{
			return $this->name;
		}

		public function get_project_id()
		{
			return $this->project_id;
		}

		public function insert()
		{
			$date_now = new DateTime(null, new DateTimeZone('Europe/Belgrade'));
            $this->set_dateandtime($date_now->format('Y-m-d H:i:s'));

			$db = new DataLayer();
			$db->query('INSERT INTO `comment` (message, dateandtime, project_id, name) VALUES (:message, :dateandtime, :project_id, :name)');
			$db->bind('message', $this->get_message());
			$db->bind('dateandtime', $this->get_dateandtime());
			$db->bind('project_id', $this->get_project_id());
			$db->bind('name', $this->get_name());
			$db->execute();
		}

		static public function select_all($project_id)
		{
			$db = new DataLayer();
			$db->query('SELECT name, message, dateandtime FROM `comment` WHERE project_id=:project_id');
			$db->bind('project_id', $project_id);
			$db->execute();

			$comments_db = array();

			if($db->rowCount() > 0)
			{
				$data = $db->resultset();

				foreach ($data as $key => $value) 
				{
					$comment = new Comment();
					$comment->set_name($value['name']);
					$comment->set_message($value['message']);
					$comment->set_dateandtime($value['dateandtime']);

					array_push($comments_db, $comment);
				}

				return $comments_db;
				
			}

			return null;
		}

	}