<?php
	class GroupHasStudent
	{
		private $group_id;
		private $group_professor_id;
		private $student_id;
		private $status;
		private $group_name;
		private $student_name;
		private $student_index;
		private $professor_name;
		private $creation_date;
		private $project_name;

		// setters
		public function set_group_id($new_group_id)
		{
			$this->group_id = $new_group_id;
		}

		public function set_group_professor_id($new_group_professor_id)
		{
			$this->group_professor_id = $new_group_professor_id;
		}

		public function set_student_id($new_student_id)
		{
			$this->student_id = $new_student_id;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		public function set_group_name($new_group_name)
		{
			$this->group_name = $new_group_name;
		}

		public function set_student_name($new_student_name)
		{
			$this->student_name = $new_student_name;
		}

		public function set_student_index($new_student_index)
		{
			$this->student_index = $new_student_index;
		}

		public function set_professor_name($new_professor_name)
		{
			$this->professor_name = $new_professor_name;
		}

        public function set_creation_date($new_creation_date)
        {
            $this->creation_date = $new_creation_date;
        }

        public function set_project_name($new_project_name)
        {
            $this->project_name = $new_project_name;
        }

		// getters
		public function get_group_id()
		{
			return $this->group_id;
		}

		public function get_group_professor_id()
		{
			return $this->group_professor_id;
		}

		public function get_student_id()
		{
			return $this->student_id;
		}

		public function get_status()
		{
			return $this->status;
		}

		public function get_group_name()
		{
			return $this->group_name;
		}

		public function get_student_name()
		{
			return $this->student_name;
		}

		public function get_student_index()
		{
			return $this->student_index;
		}

		public function get_professor_name()
		{
			return $this->professor_name;
		}

        public function get_creation_date()
        {
            return $this->creation_date;
        }

        public function get_project_name()
        {
            return $this->project_name;
        }

		// Request access to group
		static public function request_group_access($group_id, $group_professor_id, $student_id)
		{
			$db = new DataLayer();
			$db->query('INSERT INTO group_has_student (group_id, group_professor_id, student_id, status) VALUES (:group_id, :group_professor_id, :student_id, 0)');
			$db->bind('group_id', $group_id);
			$db->bind('group_professor_id', $group_professor_id);
			$db->bind('student_id', $student_id);
			$db->execute();
		}

		// Student - Group status
		static public function group_status($student_id, $group_id)
		{
			$db = new DataLayer();
			$db->query('SELECT status FROM group_has_student WHERE group_id=:group_id AND student_id=:student_id');
			$db->bind('group_id', $group_id);
			$db->bind('student_id', $student_id);
			$db->execute();

			if($db->rowCount() > 0)
				return $db->single()['status'];

			return null;
		}

		// Select by professor id
		static public function select_by_professor($professor_id)
		{
			$db = new DataLayer();
			$db->query('SELECT group_has_student.group_id AS group_id, group_has_student.group_professor_id AS group_professor_id, group_has_student.status AS status,
							group_has_student.student_id AS student_id, student.name AS student_name, student.index_number AS student_index, group.name AS group_name
							FROM group_has_student JOIN student ON group_has_student.student_id=student.id
							JOIN `group` ON group_has_student.group_id = `group`.id
							WHERE group_has_student.group_professor_id=:professor_id AND group_has_student.status=0');
			$db->bind('professor_id', $professor_id);
			$db->execute();

			$data_output = array();

			if($db->rowCount() > 0)
			{
				$data = $db->resultset();

				foreach ($data as $key => $value) 
				{
					$group_has_student = new GroupHasStudent();
					$group_has_student->set_group_id($value['group_id']);
					$group_has_student->set_group_professor_id($value['group_professor_id']);
					$group_has_student->set_student_id($value['student_id']);
					$group_has_student->set_status($value['status']);
					$group_has_student->set_group_name($value['group_name']);
					$group_has_student->set_student_name($value['student_name']);
					$group_has_student->set_student_index($value['student_index']);

					array_push($data_output, $group_has_student);
				}
			}

			return $data_output;
		}

		// Select by student id
		static public function select_by_student($student_id)
		{
			$db = new DataLayer();
			$db->query('SELECT group_has_student.group_id AS group_id, group_has_student.group_professor_id AS group_professor_id, group_has_student.status AS status,
							group_has_student.student_id AS student_id, student.name AS student_name, student.index_number AS student_index, group.name AS group_name,
							professor.name AS professor_name
							FROM group_has_student JOIN student ON group_has_student.student_id=student.id
							JOIN `group` ON group_has_student.group_id = `group`.id
							JOIN `professor` ON group_has_student.group_professor_id = professor.id
							WHERE group_has_student.student_id=:student_id AND group_has_student.status=1');
			$db->bind('student_id', $student_id);
			$db->execute();

			$data_output = array();

			if($db->rowCount() > 0)
			{
				$data = $db->resultset();

				foreach ($data as $key => $value) 
				{
					$group_has_student = new GroupHasStudent();
					$group_has_student->set_group_id($value['group_id']);
					$group_has_student->set_group_professor_id($value['group_professor_id']);
					$group_has_student->set_student_id($value['student_id']);
					$group_has_student->set_status($value['status']);
					$group_has_student->set_group_name($value['group_name']);
					$group_has_student->set_student_name($value['student_name']);
					$group_has_student->set_student_index($value['student_index']);
					$group_has_student->set_professor_name($value['professor_name']);

					array_push($data_output, $group_has_student);
				}
			}

			return $data_output;
		}

		// Process student group request
		public function update()
		{
			$db = new DataLayer();
			$db->query('UPDATE group_has_student SET status=:status WHERE group_id=:group_id AND student_id=:student_id');
			$db->bind('status', $this->get_status());
			$db->bind('group_id', $this->get_group_id());
			$db->bind('student_id', $this->get_student_id());
			$db->execute();
		}


		// Get student's groups -- ISPARVKA
		static public function get_groups($student_id)
		{
			$db = new DataLayer();
			$db->query('SELECT `group`.id, `group`.name, `group`.creation_date AS creation_date, `professor`.name AS professor_name
                              FROM `group` JOIN group_has_student ON group_has_student.group_id = `group`.id
                              JOIN `professor` ON `group_has_student`.group_professor_id=`professor`.id
                              WHERE group_has_student.student_id=:student_id AND group_has_student.status=1');

			$db->bind('student_id', $student_id);
			$db->execute();

			if($db->rowCount() > 0)
			{
				$data = $db->resultset();
				$groups = array();

				foreach ($data as $key => $value) 
				{
					$group_has_student = new GroupHasStudent();
					$group_has_student->set_group_id($value['id']);
					$group_has_student->set_group_name($value['name']);
					$group_has_student->set_professor_name($value['professor_name']);
					$group_has_student->set_creation_date($value['creation_date']);

					array_push($groups, $group_has_student);
				}

				return $groups;
			}

			return null;
		}

		// Get student's groups -- ISPARVKA
		static public function is_member($student_id, $group_id)
		{
			$db = new DataLayer();
			$db->query('SELECT student_id FROM group_has_student WHERE student_id=:student_id AND group_id=:group_id AND status=1');

			$db->bind('student_id', $student_id);
			$db->bind('group_id', $group_id);

			$db->execute();

			if($db->rowCount() > 0)
				return true;

			return false;
		}
	}
