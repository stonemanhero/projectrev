<?php
	class ProjectHasStudent
	{
		private $project_id;
		private $project_group_id;
		private $project_group_professor_id;
		private $project_student_id;
		private $student_id;

		private $student_name;
		private $student_index_number;

		// setters
		public function set_project_id($new_project_id)
		{
			$this->project_id = $new_project_id;
		}

		public function set_project_group_id($new_project_group_id)
		{
			$this->project_group_id = $new_project_group_id;
		}

		public function set_project_group_professor_id($new_project_group_professor_id)
		{
			$this->project_group_professor_id = $new_project_group_professor_id;
		}

		public function set_project_student_id($new_project_student_id)
		{
			$this->project_student_id = $new_project_student_id;
		}

		public function set_student_id($new_student_id)
		{
			$this->student_id = $new_student_id;
		}

		public function set_student_name($new_student_name)
		{
			$this->student_name = $new_student_name;
		}

		public function set_student_index_number($new_student_index_number)
		{
			$this->student_index_number = $new_student_index_number;
		}

		// getters
		public function get_project_id()
		{
			return $this->project_id;
		}

		public function get_project_group_id()
		{
			return $this->project_group_id;
		}

		public function get_project_group_professor_id()
		{
			return $this->project_group_professor_id;
		}

		public function get_project_student_id()
		{
			return $this->project_student_id;
		}

		public function get_student_id()
		{
			return $this->student_id;
		}

		public function get_student_name()
		{
			return $this->student_name;
		}

		public function get_student_index_number()
		{
			return $this->student_index_number;
		}


		static public function is_member($student_id, $project_id)
		{
			$db = new DataLayer();
			$db->query('SELECT project_id FROM project_has_student WHERE student_id=:student_id AND project_id=:project_id');
			$db->bind('student_id', $student_id);
			$db->bind('project_id', $project_id);
			$db->execute();

			if($db->rowCount() > 0)
				return true;

			return false;
		}


		public function insert()
		{
			$db = new DataLayer();
			$db->query('INSERT INTO project_has_student (project_id, project_group_id, project_group_professor_id, project_student_id, student_id) 
									VALUES (:project_id, :project_group_id, :project_group_professor_id, :project_student_id, :student_id)');
			$db->bind('project_id', $this->get_project_id());
			$db->bind('project_group_id', $this->get_project_group_id());
			$db->bind('project_group_professor_id', $this->get_project_group_professor_id());
			$db->bind('project_student_id', $this->get_project_student_id());
			$db->bind('student_id', $this->get_student_id());

			$db->execute();
		}

		public function remove()
		{
			$db = new DataLayer();
			$db->query('DELETE FROM `project_has_student` WHERE student_id=:student_id AND project_id=:project_id');
			$db->bind('project_id', $this->get_project_id());
			$db->bind('student_id', $this->get_student_id());
			$db->execute();
		}

		// Get all projects by student
        static public function get_members($id)
        {
            $db = new DataLayer();
            $db->query('SELECT `student`.name, `student`.index_number, `project_has_student`.student_id AS student_id, 
            				`project_has_student`.project_id AS project_id
            				FROM `project_has_student` 
            				JOIN `student` ON `project_has_student`.student_id = `student`.id 
            				WHERE `project_has_student`.project_id=:project_id');
            $db->bind('project_id', $id);
            $db->execute();

            $project_has_students_db = array();

            if($db->rowCount() > 0)
            {
                $data = $db->resultset();

                foreach ($data as $key => $value)
                {
                    $project_has_student = new ProjectHasStudent();
                    $project_has_student->set_student_name($value['name']);
                    $project_has_student->set_student_index_number($value['index_number']);
                    $project_has_student->set_project_id($value['project_id']);
                    $project_has_student->set_student_id($value['student_id']);

                    array_push($project_has_students_db, $project_has_student);
                }
            }

            return $project_has_students_db;
        }


        static public function select_by_student($id)
        {
            $db = new DataLayer();
            $db->query('SELECT `project`.id AS project_id, `project`.name AS project_name, `project`.creation_date AS creation_date,
                              `group`.name AS group_name
                              FROM `project_has_student` JOIN `project` ON `project_has_student`.project_id = `project`.id
                              JOIN `group` ON `project`.group_id = `group`.id 
                              WHERE `project_has_student`.student_id=:id');
            $db->bind('id', $id);
            $db->execute();

            $projects_db = array();

            if($db->rowCount() > 0)
            {
                $data = $db->resultset();

                foreach ($data as $key => $value)
                {
                    $project = new Project();
                    $project->set_id($value['project_id']);
                    $project->set_name($value['project_name']);
                    $project->set_creation_date($value['creation_date']);
                    $project->set_group_name($value['group_name']);

                    array_push($projects_db, $project);
                }
            }

            return $projects_db;
        }
	}