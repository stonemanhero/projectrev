<?php
	class Professor
	{
		private $email;
		private $password;
		private $name;
		private $status;

		// setters
		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_email($new_email)
		{
			$this->email = $new_email;
		}

		public function set_password($new_password)
		{
			$this->password = password_hash($new_password, PASSWORD_DEFAULT);
		}

		public function set_name($new_name)
		{
			$this->name = $new_name;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		// getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_email()
		{
			return $this->email;
		}

		public function get_password()
		{
			return $this->password;
		}

		public function get_name()
		{
			return $this->name;
		}

		public function get_status()
		{
			return $this->status;
		}

		// Sign in Student
		static public function login($email, $password)
		{	
			$db = new DataLayer();
			$db->query('SELECT id, name, password FROM professor WHERE email=:email AND status=1');
			$db->bind('email', $email);
			$db->execute();

			if($db->rowCount() > 0)
			{
				if($db->rowCount() > 0)
				{
					if(password_verify($password, $db->single()['password']))
					{
						$professor = new Professor();
						$professor->set_id($db->single()['id']);
						$professor->set_name($db->single()['name']);

						return $professor;
					}
				}
			}
			return null;
		}
	}
?>