<?php
	class Student
	{
		private $id;
		private $email;
		private $password;
		private $name;
		private $index_number;
		private $status;

		// setters
		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_email($new_email)
		{
			$this->email = $new_email;
		}

		public function set_password($new_password)
		{
			$this->password = password_hash($new_password, PASSWORD_DEFAULT);
		}

		public function set_name($new_name)
		{
			$this->name = $new_name;
		}

		public function set_index_number($new_index_number)
		{
			$this->index_number = $new_index_number;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		// getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_email()
		{
			return $this->email;
		}

		public function get_password()
		{
			return $this->password;
		}

		public function get_name()
		{
			return $this->name;
		}

		public function get_index_number()
		{
			return $this->index_number;
		}

		public function get_status()
		{
			return $this->status;
		}

		// Sign in Student
		static public function login($email, $password)
		{	
			$db = new DataLayer();
			$db->query('SELECT id, name, email, password FROM student WHERE email=:email AND status=1');
			$db->bind('email', $email);
			$db->execute();

			if($db->rowCount() > 0)
			{
				if(password_verify($password, $db->single()['password']))
				{
					$student = new Student();
					$student->set_id($db->single()['id']);
					$student->set_name($db->single()['name']);
					$student->set_email($db->single()['email']);

					return $student;
				}
			}

			return null;
		}

		// Check if student already registered
		static public function available($email)
		{
			$db = new DataLayer();
			$db->query('SELECT id FROM student WHERE email=:email');
			$db->bind('email', $email);
			$db->execute();

			if($db->rowCount() > 0)
				return false;

			return true;
		}

		// Get studen id from email
		static public function find_id($email)
		{
			$db = new DataLayer();
			$db->query('SELECT id FROM student WHERE email=:email');
			$db->bind('email', $email);
			$db->execute();

			if($db->rowCount() > 0)
				return $db->single()['id'];

			return -1;
		}

		// Register new Student
		public function register()
		{
			$db = new DataLayer();
			$db->query('INSERT INTO student (email, password, name, index_number) VALUES (:email, :password, :name, :index_number)');
			$db->bind('email', $this->email);
			$db->bind('name', $this->name);
			$db->bind('password', $this->password);
			$db->bind('name', $this->name);
			$db->bind('index_number', $this->index_number);
			$db->execute();
		}
	}
?>