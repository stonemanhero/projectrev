<?php
	class Project
	{
		private $id;
		private $name;
		private $description;
		private $link;
		private $creation_date;
		private $group_id;
		private $group_name;
		private $group_professor_id;
		private $group_professor_name;
		private $student_id;
		private $student_name;
		private $status;

		// setters
		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_name($new_name)
		{
			$this->name = $new_name;
		}

		public function set_description($new_description)
		{
			$this->description = $new_description;
		}

		public function set_link($new_link)
		{
			$this->link = $new_link;
		}

		public function set_creation_date($new_creation_date)
		{
			$this->creation_date = $new_creation_date;
		}

		public function set_group_id($new_group_id)
		{
			$this->group_id = $new_group_id;
		}

        public function set_group_name($new_group_name)
        {
            $this->group_name = $new_group_name;
        }

		public function set_group_professor_id($new_group_professor_id)
		{
			$this->group_professor_id = $new_group_professor_id;
		}

        public function set_group_professor_name($new_group_professor_name)
        {
            $this->group_professor_name = $new_group_professor_name;
        }

		public function set_student_id($new_student_id)
		{
			$this->student_id = $new_student_id;
		}

		public function set_student_name($new_student_name)
		{
			$this->student_name = $new_student_name;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		// getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_name()
		{
			return $this->name;
		}

		public function get_description()
		{
			return $this->description;
		}

		public function get_link()
		{
			return $this->link;
		}

		public function get_creation_date()
		{
			return $this->creation_date;
		}

		public function get_group_id()
		{
			return $this->group_id;
		}

        public function get_group_name()
        {
            return $this->group_name;
        }

        public function get_group_professor_id()
        {
            return $this->group_professor_id;
        }

		public function get_group_professor_name()
		{
			return $this->group_professor_name;
		}

		public function get_student_id()
		{
			return $this->student_id;
		}

		public function get_student_name()
		{
			return $this->student_name;
		}

		public function get_status()
		{
			return $this->status;
		}

		// Check if project name available
		static public function available($name, $group_id)
		{
			$db = new DataLayer();
			$db->query("SELECT id FROM `project` WHERE name=:name AND group_id=:group_id");
			$db->bind('name', $name);
            $db->bind('group_id', $group_id);
			$db->execute();

			if($db->rowCount() > 0)
				return false;

			return true;
		}

        // Check if user is project owner
        static public function owner($project_id, $student_id)
        {
            $db = new DataLayer();
            $db->query("SELECT id FROM `project` WHERE id=:id AND student_id=:student_id");
            $db->bind('id', $project_id);
            $db->bind('student_id', $student_id);
            $db->execute();

            if($db->rowCount() > 0)
                return true;

            return false;
        }

		// Get all projects by group
		static public function select_by_group($id)
		{
			$db = new DataLayer();
			$db->query('SELECT `project`.id AS project_id, `project`.name AS project_name, `project`.creation_date AS creation_date, 
                              `student`.name AS student_name FROM `project` JOIN `group` ON `project`.group_id=`group`.id 
                              JOIN student ON `project`.student_id=`student`.id  WHERE `project`.group_id=:id AND `project`.status=1');
			$db->bind('id', $id);
			$db->execute();

			$projects_db = array();

			if($db->rowCount() > 0)
			{
				$data = $db->resultset();

				foreach ($data as $key => $value) 
				{
					$project = new Project();
					$project->set_id($value['project_id']);
					$project->set_name($value['project_name']);
					$project->set_student_name($value['student_name']);
                    $project->set_creation_date($value['creation_date']);

					array_push($projects_db, $project);
				}
			}

			return $projects_db;
		}

        // Get all project requests
        static public function get_requests($id)
        {
            $db = new DataLayer();
            $db->query('SELECT `project`.id AS project_id, `project`.name AS project_name, `project`.creation_date AS creation_date, 
                            `student`.name AS student_name FROM `project` JOIN `group` ON `project`.group_id=`group`.id 
                            JOIN student ON `project`.student_id=`student`.id  WHERE `project`.group_professor_id=:id AND `project`.status=0');
            $db->bind('id', $id);
            $db->execute();

            $projects_db = array();

            if($db->rowCount() > 0)
            {
                $data = $db->resultset();

                foreach ($data as $key => $value)
                {
                    $project = new Project();
                    $project->set_id($value['project_id']);
                    $project->set_name($value['project_name']);
                    $project->set_student_name($value['student_name']);
                    $project->set_creation_date($value['creation_date']);

                    array_push($projects_db, $project);
                }
            }

            return $projects_db;
        }

        // Get all projects by student
        static public function select_by_student($id)
        {
            $db = new DataLayer();
            $db->query('SELECT `project`.id AS project_id, `project`.name AS project_name, `project`.creation_date AS creation_date,
                              `group`.name AS group_name
                              FROM `project` JOIN `group` ON `project`.group_id = `group`.id 
                              WHERE `project`.student_id=:id AND `project`.status=1');
            $db->bind('id', $id);
            $db->execute();

            $projects_db = array();

            if($db->rowCount() > 0)
            {
                $data = $db->resultset();

                foreach ($data as $key => $value)
                {
                    $project = new Project();
                    $project->set_id($value['project_id']);
                    $project->set_name($value['project_name']);
                    $project->set_creation_date($value['creation_date']);
                    $project->set_group_name($value['group_name']);

                    array_push($projects_db, $project);
                }
            }

            return $projects_db;
        }

		// Insert new project
        public function insert()
        {
            $date_now = new DateTime(null, new DateTimeZone('Europe/Belgrade'));
            $this->set_creation_date($date_now->format('Y-m-d H:i:s'));

            $db = new DataLayer();
            //error_log($this->name . ' ' . $this->description . ' ' . $this->creation_date . ' ' . $this->group_professor_id . ' ' . $this->link . ' ' . $this->group_id . ' ' . $this->student_id);
            $db->query('INSERT INTO project (name, description, link, creation_date, group_id, group_professor_id, student_id) 
                              VALUES (:name, :description, :link, :creation_date, :group_id, :group_professor_id, :student_id)');
            $db->bind('name', $this->name);
            $db->bind('description', $this->description);
            $db->bind('creation_date', $this->creation_date);
            $db->bind('group_professor_id', $this->group_professor_id);
            $db->bind('link', $this->link);
            $db->bind('group_id', $this->group_id);
            $db->bind('student_id', $this->student_id);
            $db->execute();

        }

        // Process student project request
        public function update()
        {
            $db = new DataLayer();
            $db->query('UPDATE project SET status=:status WHERE id=:id');
            $db->bind('status', $this->get_status());
            $db->bind('id', $this->get_id());
            $db->execute();
        }

        // Update edit project
        public function update_edit()
        {
            $db = new DataLayer();
            $db->query('UPDATE `project` SET name=:name, link=:link, description=:description WHERE id=:id');
            $db->bind('name', $this->name);
            $db->bind('link', $this->link);
            $db->bind('description', $this->description);
            $db->bind('id', $this->id);
            $db->execute();
        }

        // Select project by id
        static public function select($id)
        {
            $db = new DataLayer();
            $db->query('SELECT `project`.id AS id, `project`.name AS project_name, `project`.description AS project_description, `project`.link AS project_link, `project`.creation_date AS project_creation_date, `group`.id AS group_id, `group`.name AS group_name, `professor`.name AS professor_name, `student`.name AS student_name, `student`.id AS leader, `professor`.id AS professor_id
                              FROM `project` JOIN `group` ON `project`.group_id=`group`.id JOIN `professor` ON `project`.group_professor_id=`professor`.id
                              JOIN `student` ON `project`.student_id=`student`.id WHERE `project`.id=:id AND `project`.status=1');
            $db->bind('id', $id);
            $db->execute();

            if($db->rowCount() > 0)
            {
                $data = $db->single();
                $project = new Project();
                $project->set_id($data['id']);
                $project->set_name($data['project_name']);
                $project->set_description($data['project_description']);
                $project->set_link($data['project_link']);
                $project->set_creation_date($data['project_creation_date']);
                $project->set_group_name($data['group_name']);
                $project->set_group_professor_name($data['professor_name']);
                $project->set_student_name($data['student_name']);
                $project->set_group_id($data['group_id']);
                $project->set_student_id($data['leader']);
                $project->set_group_professor_id($data['professor_id']);

                return $project;
            }

            return null;
        }
	}