<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassStudent.php';
    include_once 'php/model/ClassProfessor.php';

    session_start();

    if(!empty($_SESSION['user']))
    {
        header("Location: index.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ProjectRev - Please, sign in</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/fashion.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container ">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 ">
                <div class="login-panel panel panel-default ">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">ProjectRev v1.0</h3>
                    </div>
                    <div class="panel-body">
                        <form id="form_login" name="form_login" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input id="input_email" class="form-control" placeholder="Email" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input id="input_password" class="form-control" placeholder="Password" name="password" type="password">
                                </div>
                                <div id="alert_info">
                                </div>
                                <button id="login_user" type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
                                <br>
                                <p class="text-center">You don't have account? <a href="register.php">Register</a></p>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Custom -->
    <script src="js/custom/login.js"></script>
  </body>
</html>