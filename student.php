<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassStudent.php';
    include_once 'php/model/ClassGroup.php';
    include_once 'php/model/ClassGroupHasStudent.php';
    include_once 'php/model/ClassProject.php';
    include_once 'php/model/ClassProjectHasStudent.php';

    session_start();

    if(empty($_SESSION['user'])) 
    {
        header('Location: login.php');
        exit();
    }
    else 
    {
        if($_SESSION['type'] === 'professor') 
        {
            header('Location: professor.php');
            exit();
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ProjectRev - Overview</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/fashion.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Main menu -->
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">ProjectRev v1.0</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="groups.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Find group</a></li>
                <?php
                    if($_SESSION['type'] == "professor")
                        print '<li><a href="create-group.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create group</a></li>';
                    else
                        print '<li><a href="create-project.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create project</a></li>';
                ?>

            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-push-8">
            <p>Welcome, <?php if(!empty($_SESSION['user'])) print $_SESSION['user']->get_name(); ?> (<a href="php/logic/logout.php">logout</a>)</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <h1 class="page-header"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> My groups</h1>
        </div>
    </div>

    <?php
        $groups = GroupHasStudent::get_groups($_SESSION['user']->get_id());

            print '<div class="row">';
        if(!empty($groups))
        {
            print '<div id="table_mygroups">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table borderless">
                                    <thead>
                                        <tr>
                                            <th><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> Name</th>
                                            <th><span class="glyphicon glyphicon-folder-user" aria-hidden="true"></span> Created by</th>
                                            <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Creation date</th>
                                            <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            foreach ($groups as $key => $value)
            {
                print '<tr>';
                print '<td>' . $value->get_group_name() . '</td>';
                print '<td>' . $value->get_professor_name() . '</td>';
                print '<td>' . $value->get_creation_date() . '</td>';
                print '<td>
                                            <a href="group.php?id=' . $value->get_group_id() . '"><button class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</button></a>
                                        </td>';
                print '</tr>';
            }

            print '                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>';
        }
        else
        {
            print '<div class="alert alert-warning">You are not involved in any group.</div>';
        }

        print '</div>';
    ?>

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> My projects (as leader)</h1>
        </div>
    </div>

    <div class="row">
        <?php
            $projects = Project::select_by_student($_SESSION['user']->get_id());

            if(!empty($projects))
            {
                print '<div id="table_mygroups">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table borderless">
                                                    <thead>
                                                        <tr>
                                                            <th><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> Project</th>
                                                            <th><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Group</th>
                                                            <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Creation date</th>
                                                            <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>';

                foreach ($projects as $key => $value)
                {
                    print '<tr>';
                    print '<td>' . $value->get_name() . '</td>';
                    print '<td>' . $value->get_group_name() . '</td>';
                    print '<td>' . $value->get_creation_date() . '</td>';
                    print '<td>
                                                <a href="project.php?id=' . $value->get_id() . '"><button class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</button></a>
                                                <a href="edit-project.php?id=' . $value->get_id() . '">
                                                    <button class="btn btn-success"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button>
                                                </a>
                                            </td>';
                    print '</tr>';
                }

                print '                            </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>';
            }
            else
            {
                print '<div class="alert alert-warning">You are not leader of any group.</div>';
            }

        ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> My projects (as team member)</h1>
        </div>
    </div>

    <div class="row">
        <?php
        $projects = ProjectHasStudent::select_by_student($_SESSION['user']->get_id());

        if(!empty($projects))
        {
            print '<div id="table_mygroups">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table borderless">
                                                    <thead>
                                                        <tr>
                                                            <th><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> Project</th>
                                                            <th><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Group</th>
                                                            <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Creation date</th>
                                                            <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>';

            foreach ($projects as $key => $value)
            {
                print '<tr>';
                print '<td>' . $value->get_name() . '</td>';
                print '<td>' . $value->get_group_name() . '</td>';
                print '<td>' . $value->get_creation_date() . '</td>';
                print '<td>
                                                <a href="project.php?id=' . $value->get_id() . '"><button class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</button></a>
                                                
                                            </td>';
                print '</tr>';
            }

            print '                            </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>';
        }
        else
        {
            print '<div class="alert alert-warning">You are not member of any group.</div>';
        }

        print '</div>';
        ?>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script src="js/custom/index.js"></script>
</body>
</html>