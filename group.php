<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassStudent.php';
    include_once 'php/model/ClassProfessor.php';
    include_once 'php/model/ClassGroup.php';
    include_once 'php/model/ClassProject.php';
    include_once 'php/model/ClassGroupHasStudent.php';

    session_start();

    if(empty($_SESSION['user']))
    {
        header("Location: login.php");
        exit();
    }
    else
    {
        $user = $_SESSION['user'];
        //$type = $_SESSION['type'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ProjectRev - Group</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/fashion.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Main menu -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ProjectRev v1.0</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="groups.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Find group</a></li>
                    <?php
                        if($_SESSION['type'] == "professor")
                            print '<li><a href="create-group.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create group</a></li>';
                        else
                            print '<li><a href="create-project.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create project</a></li>';
                    ?>
              </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-push-8">
                <p>Welcome, <?php print $user->get_name(); ?> (<a href="php/logic/logout.php">logout</a>)</p>
            </div>
        </div>

        <?php
            if(!empty($_GET['id']))
                $group = Group::select($_GET['id']);

            function group_view($group)
            {
                print '<div class="row">
                            <div class="col-md-12">
                                <h1 class="page-header"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> ' . $group->get_name();

                                if($_SESSION['type'] == "professor" && Group::owner($group->get_id(), $_SESSION['user']->get_id()))
                                    print ' <a href="edit-group.php?id=' . $group->get_id() . '"><button class="btn btn-success"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button></a>';
                                
                                print '<br><span class="author">Created by ' . $group->get_professor_name() .'</span></h1>
                            </div>

                            <div class="col-md-12">
                            <br>
                                ' . $group->get_about() . '
                            </div>
                        </div>';

                print '<div class="row">
                            <div class="col-md-12">
                                <br><br>
                                <h1 class="page-header"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Projects</h1>
                            </div>
                        </div>
                        <div class="row">';
                        $projects = Project::select_by_group($group->get_id());

                        if(!empty($projects))
                        {
                            print '<div id="table_mygroups">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table borderless">
                                            <thead>
                                                <tr>
                                                    <th><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> Group</th>
                                                    <th><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Leader</th>
                                                    <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Creation date</th>
                                                    <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>';

                            foreach ($projects as $key => $value)
                            {
                                print '<tr>';
                                print '<td>' . $value->get_name() . '</td>';
                                print '<td>' . $value->get_student_name() . '</td>';
                                print '<td>' . $value->get_creation_date() . '</td>';
                                print '<td>
                                        <a href="project.php?id=' . $value->get_id() . '"><button class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</button></a>
                                    </td>';
                                print '</tr>';
                            }

                            print '                            </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>';
                        }
                        else
                            print '<div class="alert alert-warning"><strong>There are no projects in this group!</strong></div>';

                print '</div>';
            }

            if(!empty($group))
            {
                if($_SESSION['type'] == "student")
                {
                    $student_group_status = GroupHasStudent::group_status($user->get_id(), $group->get_id());

                    if(is_null($student_group_status))
                    {
                        print '<div class="alert alert-warning"><strong>You don\'t have access to this group!</strong></div>';
                        print '<button data-professor-id="' . $group->get_professor_id() . '" data-group-id="' . $group->get_id() . '" data-user-id="' . $user->get_id() . '" id="request_group_access" class="btn btn-lg btn-primary">Request access</button>';
                    }
                    else if($student_group_status == 0)
                    {
                        print '<div class="alert alert-success"><strong>Your access request is in process.</strong>';
                    }
                    else if($student_group_status == -1)
                    {
                        print '<div class="alert alert-danger"><strong>Professor deny your access group request.</strong>';
                    }
                    else
                    {
                        group_view($group);
                    }
                }
                else
                {
                    group_view($group);
                }
            }
            else
            {
                print '<div class="alert alert-warning"><strong>Warning! This group doesn\'t exist!</div>';
            }
        ?>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script src="js/custom/group.js"></script>
</body>
</html>