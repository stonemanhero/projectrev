$(function(){

	function clear_inputs()
	{
		$('#alert_error').text("");
	}

	// Register user
	$("#login_user").click(function(event){
		event.preventDefault();
		clear_inputs();

		var email = $('#input_email').val();
		var password = $('#input_password').val();
    	var ajaxRequest;
      	var values = {email: email, password: password};

       	ajaxRequest= $.ajax({
	        url: "php/logic/login_user.php",
	        type: "post",
	        data: values,

	        success:function(data, textStatus, jqXHR) {
	        	var data = jQuery.parseJSON(data);
		        if(data.info != null)
		        {
		        	$('#alert_info').html('<div class="alert alert-success"> <strong>Success!</strong> You have been successfully signed in!</div>');
		        	$('#form_login')[0].reset();
		        	window.setTimeout(function(){location.reload()}, 1000);
		        }

		        else if(data.error != null)
		        	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
		   },
		 
		 	error:function(jqXHR, textStatus, errorThrown) {
		    	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + errorThrown + '</div>');
		 	}
	    });
	});
});