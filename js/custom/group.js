$(function(){
	$('#request_group_access').on('click', function(event){
		event.preventDefault();
		
		var user_id = $(this).data('user-id');
		var group_id = $(this).data('group-id');
		var professor_id = $(this).data('professor-id');

		var ajaxRequest;
	    var values = {user_id: user_id, group_id: group_id, professor_id: professor_id};

	    ajaxRequest= $.ajax({
	        url: "php/logic/request_group_access.php",
	        type: "post",
	        data: values,

	        success:function(data, textStatus, jqXHR) {
	        	var data = jQuery.parseJSON(data);
		        if(data.info != null)
		        {
		        	window.setTimeout(function(){location.reload()}, 100);
		        }
		        else
		        {
		        	alert('Error - implement this!');
		        }
		   },
		 
		 	error:function(jqXHR, textStatus, errorThrown) {
		    	alert('Error - implement this!');
		 	}
	    }); 
	});
});