/**
 * Created by stone on 12.5.17..
 */
$(function(){

    function load_data()
    {
        var project_id = -1;
        project_id = $('#input_project_id').val();

        if(project_id > -1) {
            $("#other_members").load('php/parts/get_team_members.php', {id:project_id}, function() {
                reload_clicks();
            });  
        }     
    }

    function clear_alerts()
    {
        $('#alert_description').text("");
        $('#alert_link').text("");
        $('#alert_name').text("");
        $('#alert_info').text("");
    }

    
    $('#edit_project_btn').on('click', function(event){
        event.preventDefault();
        clear_alerts();

        var name = $('#input_name').val();
        var link =  $('#input_link').val();
        var description = $('#input_description').val();
        var project_id = $('#input_project_id').val();

        var ajaxRequest;
        var values = {name: name, link: link, description: description, project_id: project_id};

        ajaxRequest= $.ajax({
            url: "php/logic/edit_project.php",
            type: "post",
            data: values,

            success:function(data, textStatus, jqXHR) {
                var data = jQuery.parseJSON(data);
                if(data.info != null)
                {
                    $('#alert_info').html('<div class="alert alert-success"> <strong>Success!</strong> Project has been successfully edited!</div>');
                }

                else if(data.name != null)
                    $('#alert_name').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.name + '</div>');

                else if(data.link != null)
                    $('#alert_link').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.link + '</div>');

                else if(data.description != null)
                    $('#alert_description').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.description + '</div>');

                else if(data.error != null) {
                    $('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
                }
            },

            error:function(jqXHR, textStatus, errorThrown) {
                $('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + errorThrown + '</div>');
            }
        });
    });


    $('#add_member_btn').on('click', function(event){
        event.preventDefault();
        clear_alerts();

        var email = $('#input_email').val();
        var group_id = $('#input_member_group_id').val();
        var project_id = $('#input_member_project_id').val();
        var group_professor_id = $('#input_member_group_professor_id').val();
        var student_id = $('#input_member_student_id').val();

        var ajaxRequest;
        var values = { email: email, group_id: group_id, project_id: project_id, group_professor_id: group_professor_id, student_id: student_id };

        ajaxRequest= $.ajax({
            url: "php/logic/add_member.php",
            type: "post",
            data: values,

            success:function(data, textStatus, jqXHR) {
                var data = jQuery.parseJSON(data);

                if(data.info != null)
                {
                    $('#alert_member_info').html('<div class="alert alert-success"> <strong>Success!</strong> Member has been successfully added to team!</div>');
                    $('#form_add_member')[0].reset();
                    load_data();
                }

                else if(data.error != null) {
                    $('#alert_member_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
                }
            },

            error:function(jqXHR, textStatus, errorThrown) {
                $('#alert_member_info').html('<div class="alert alert-danger"><strong>Internal server error!</strong></div>');
            }
        });
    });

    function reload_clicks() {
        $('.remove-member').on('click', function(event) {
            event.preventDefault();

            var project_id = $(this).data('project_id');
            var student_id = $(this).data('student_id');

            var ajaxRequest;
            var values = { project_id: project_id, student_id: student_id };

            ajaxRequest= $.ajax({
                url: "php/logic/remove_member.php",
                type: "post",
                data: values,

                success:function(data, textStatus, jqXHR) {
                    var data = jQuery.parseJSON(data);

                    if(data.info != null)
                    {
                        $('#alert_member_info').html('<div class="alert alert-success"> <strong>Success!</strong> Member has been successfully removed from team!</div>');
                        load_data();
                    }

                    else if(data.error != null) {
                        $('#alert_member_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
                    }
                },

                error:function(jqXHR, textStatus, errorThrown) {
                    $('#alert_member_info').html('<div class="alert alert-danger"><strong>Internal server error!</strong></div>');
                }
            });
        });
    };

    load_data();
});