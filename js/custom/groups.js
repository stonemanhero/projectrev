$(function(){
	$('#input_group_search').on('input', function(){
		var search_text = $('#input_group_search').val();

		if(search_text.length > 2)
		{
			var ajaxRequest;
		    var values = {search_text: search_text};

		    ajaxRequest= $.ajax({
		        url: "php/logic/find_group.php",
		        type: "post",
		        data: values,

		        success:function(data, textStatus, jqXHR) {
		        	var data = jQuery.parseJSON(data);
			        if(data.info != null)
			        {
			        	$('#search_results').html(data.info);
			        }
			        else
			        {
			        	alert('Error - implement this!');
			        }
			   },
			 
			 	error:function(jqXHR, textStatus, errorThrown) {
			    	alert('Error - implement this!');
			 	}
		    }); 
		}
	});
});