$(function(){

	function clear_alerts()
	{
		$('#alert_group').text("");
		$('#alert_name').text("");
		$('#alert_info').text("");
	}

	$('#create_project_btn').on('click', function(event){
		event.preventDefault();
		clear_alerts();
		
		var name = $('#input_name').val();
		var group_id = $('#input_group').val();
		var link = $('#input_link').val();
		var description = quill.container.firstChild.innerHTML;

		var ajaxRequest;
	    var values = {name: name, group_id: group_id, link: link, description: description};

	    ajaxRequest = $.ajax({
	        url: "php/logic/create_project.php",
	        type: "post",
	        data: values,

	        success:function(data, textStatus, jqXHR) {
	        	var data = jQuery.parseJSON(data);
		        if(data.info != null)
		        {
		        	$('#alert_info').html('<div class="alert alert-success"> <strong>Success!</strong> Project has been successfully sent to professor on review.</div>');
		        	$('#form_create_group')[0].reset();
		        	quill.setText("");
		        }

		        else if(data.name != null)
		        	$('#alert_name').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.name + '</div>');

		        else if(data.group_id != null)
		        	$('#alert_group_id').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.group_id + '</div>');

		        else if(data.error != null)
		        	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
		   },
		 
		 	error:function(jqXHR, textStatus, errorThrown) {
		    	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + errorThrown + '</div>');
		 	}
	    }); 
	});
});