$(function(){

	function clear_alerts()
	{
		$('#alert_password').text("");
		$('#alert_name').text("");
		$('#alert_email').text("");
		$('#alert_index_number').text("");
		$('#alert_info').text("");
	}

	// Register user
	$("#register_student").click(function(event){
		event.preventDefault();
		clear_alerts();

		var email = $('#input_email').val();
		var password = $('#input_password').val();
		var password2 = $('#input_password2').val();
		var name = $('#input_name').val();
		var index_number = $('#input_index_number').val();

		if(password === password2)
		{
	    	var ajaxRequest;
	      	var values = {email: email, password: password, password2: password2, name: name, index_number: index_number};

	       	ajaxRequest= $.ajax({
		        url: "php/logic/register_student.php",
		        type: "post",
		        data: values,

		        success:function(data, textStatus, jqXHR) {
		        	var data = jQuery.parseJSON(data);
			        if(data.info != null)
			        {
			        	$('#alert_info').html('<div class="alert alert-success"> <strong>Success!</strong> You have been successfully registered!</div>');
			        	$('#form_register')[0].reset();
			        	window.setTimeout(function(){window.location.href = "login.php"}, 2000);
			        }
			        
			        else if(data.email != null)
			        	$('#alert_email').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.email + '</div>');

			        else if(data.password != null)
			        	$('#alert_password').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.password + '</div>');

			        else if(data.name != null)
			        	$('#alert_name').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.name + '</div>');

			        else if(data.index_number != null)
			        	$('#alert_index_number').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.index_number + '</div>');

			        else if(data.error != null)
			        	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
			   },
			 
			 	error:function(jqXHR, textStatus, errorThrown) {
			    	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + errorThrown + '</div>');
			 	}
		    });
   		}
   		else
   		{
   			$('#alert_password').html('<div class="alert alert-danger"><strong>Error!</strong> Passwords doesn\'t match!</div>');
   		}
	});
});