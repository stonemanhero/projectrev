$(function(){

	function clear_alerts()
	{
		$('#alert_about').text("");
		$('#alert_name').text("");
		$('#alert_info').text("");
	}

	$('#create_group_btn').on('click', function(event){
		event.preventDefault();
		clear_alerts();
		
		var name = $('#input_name').val();
		var about = quill.container.firstChild.innerHTML;

		var ajaxRequest;
	    var values = {name: name, about: about};

	    ajaxRequest= $.ajax({
	        url: "php/logic/create_group.php",
	        type: "post",
	        data: values,

	        success:function(data, textStatus, jqXHR) {
	        	var data = jQuery.parseJSON(data);
		        if(data.info != null)
		        {
		        	$('#alert_info').html('<div class="alert alert-success"> <strong>Success!</strong> Group has been successfully created!</div>');
		        	$('#form_create_group')[0].reset();
		        	quill.setText("");
		        }

		        else if(data.name != null)
		        	$('#alert_name').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.name + '</div>');

		        else if(data.about != null)
		        	$('#alert_about').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.about + '</div>');

		        else if(data.error != null)
		        	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
		   },
		 
		 	error:function(jqXHR, textStatus, errorThrown) {
		    	$('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + errorThrown + '</div>');
		 	}
	    }); 
	});
});