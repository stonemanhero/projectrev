/**
 * Created by stone on 12.5.17..
 */
$(function(){

    function clear_alerts()
    {
        $('#alert_about').text("");
        $('#alert_name').text("");
        $('#alert_info').text("");
    }

    $('#edit_group_btn').on('click', function(event){
        event.preventDefault();
        clear_alerts();

        var name = $('#input_name').val();
        var about = $('#input_about').val();
        var group_id = $('#input_group_id').val();

        var ajaxRequest;
        var values = {name: name, about: about, group_id: group_id};

        ajaxRequest= $.ajax({
            url: "php/logic/edit_group.php",
            type: "post",
            data: values,

            success:function(data, textStatus, jqXHR) {
                var data = jQuery.parseJSON(data);
                if(data.info != null)
                {
                    $('#alert_info').html('<div class="alert alert-success"> <strong>Success!</strong> Group has been successfully edited!</div>');
                }

                else if(data.name != null)
                    $('#alert_name').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.name + '</div>');

                else if(data.about != null)
                    $('#alert_about').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.about + '</div>');

                else if(data.error != null)
                    $('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
            },

            error:function(jqXHR, textStatus, errorThrown) {
                $('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + errorThrown + '</div>');
            }
        });
    });
});