/**
 * Created by stone on 12.5.17..
 */
$(function(){

    function load_data()
    {
        var project_id = -1;
        project_id = $('#input_project_id').val();

        if(project_id > -1) {
            $("#comments").load('php/parts/get_comments.php', {project_id:project_id}, function() {

            });  
        }     
    }

    function clear_alerts()
    {
        $('#alert_info').text("");
    }

    $('#add_comment').on('click', function(event){
        event.preventDefault();
        clear_alerts();
        
        var project_id = $('#input_project_id').val();
        var name = $('#input_name').val();
        var message = $('#input_message').val();

        var ajaxRequest;
        var values = {project_id:project_id, name:name, message:message};

        ajaxRequest = $.ajax({
            url: "php/logic/add_comment.php",
            type: "post",
            data: values,

            success:function(data, textStatus, jqXHR) {
                var data = jQuery.parseJSON(data);
                if(data.info != null)
                {
                    $('#alert_info').html('<div class="alert alert-success"> <strong>Success!</strong> Project has been successfully sent to professor on review.</div>');
                    $('#form_login')[0].reset();

                    load_data();
                }

                else if(data.error != null)
                    $('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + data.error + '</div>');
           },
         
            error:function(jqXHR, textStatus, errorThrown) {
                $('#alert_info').html('<div class="alert alert-danger"><strong>Error!</strong> ' + errorThrown + '</div>');
            }
        }); 
    });

    load_data();
});