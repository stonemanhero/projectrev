$(function(){
	$('.process_group_request').on('click', function(event){
		event.preventDefault();
		
		var group_id = $(this).data('group-id');
		var student_id = $(this).data('student-id');
		var status = $(this).data('status');

		var ajaxRequest;
	    var values = {group_id: group_id, student_id: student_id, status: status};

	    ajaxRequest= $.ajax({
	        url: "php/logic/process_group_request.php",
	        type: "post",
	        data: values,

	        success:function(data, textStatus, jqXHR) {
	        	var data = jQuery.parseJSON(data);
		        if(data.info != null)
		        {
		        	window.setTimeout(function(){location.reload()}, 100);
		        }
		        else
		        {
		        	alert('Error - implement this!');
		        }
		   },
		 
		 	error:function(jqXHR, textStatus, errorThrown) {
		    	alert('Error - implement this!');
		 	}
	    }); 
	});

    $('.process_project_request').on('click', function(event){
        event.preventDefault();

        var project_id = $(this).data('project-id');
        var status = $(this).data('status');

        var ajaxRequest;
        var values = { project_id: project_id, status: status };

        ajaxRequest= $.ajax({
            url: "php/logic/process_project_request.php",
            type: "post",
            data: values,

            success:function(data, textStatus, jqXHR) {
                var data = jQuery.parseJSON(data);
                if(data.info != null)
                {
                    window.setTimeout(function(){location.reload()}, 100);
                }
                else
                {
                    alert('Error - implement this!');
                }
            },

            error:function(jqXHR, textStatus, errorThrown) {
                alert('Error - implement this!');
            }
        });
    });
});