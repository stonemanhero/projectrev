<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassStudent.php';
    include_once 'php/model/ClassGroup.php';
    include_once 'php/model/ClassGroupHasStudent.php';
    include_once 'php/model/ClassProject.php';
    include_once 'php/model/ClassProjectHasStudent.php';

    session_start();

    if(empty($_SESSION['user'])) 
    {
        header('Location: login.php');
        exit();
    }
    else 
    {
        if($_SESSION['type'] === 'professor') 
        {
            header('Location: professor.php');
            exit();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ProjectRev - Edit project</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom -->
    <link href="css/fashion.css" rel="stylesheet">
    <!-- Quill -->
    <link rel="stylesheet" href="css/quill.snow.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Main menu -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ProjectRev v1.0</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="create-project.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create project</a></li>
              </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-push-8">
                <p>Welcome, <?php if(!empty($_SESSION['user'])) print $_SESSION['user']->get_name(); ?> (<a href="php/logic/logout.php">logout</a>)</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Edit project</h1>
            </div>
        </div>

        <div class="row">
            <?php
                $id = -1;
                if(isset($_GET['id']))
                    $id = $_GET['id'];

                $project = Project::select($id);

                if(!empty($project)) {
                    // Edit project
                    print '<div class="col-md-8 col-md-push-2">
                                <br><br>
                                <form id="form_edit_project" name="form_edit_project" role="form">
                                    <div>
                                      <p class="pagealerts text-center">Make your changes...</p>
                                    </div>
                                    <fieldset>
                                        <input id="input_project_id" type="hidden" name="project_id" value="' . $project->get_id() . '">
                                        <div class="form-group">
                                            <div id="alert_name"></div>
                                            <input class="form-control" placeholder="Name" name="name" id="input_name" type="text" autofocus value="';
                                            print $project->get_name() . '">';

                                print '</div>
                                        
                                        <div class="form-group">
                                            <div id="alert_link"></div>
                                            <input class="form-control" placeholder="Link" name="link" id="input_link" type="url" value="';
                                            print $project->get_link() . '">';

                                print '</div>
                                        <div class="form-group">
                                            <div id="alert_description"></div>
                                            <textarea class="form-control" name="about" id="input_description" placeholder="About" rows="10">';

                                                    print $project->get_description();

                                            print '</textarea>
                                        </div>
                                        
                                        <div id="alert_info"></div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <button id="edit_project_btn" type="submit" class="btn btn-lg btn-primary btn-block">Save</button>
                                    </fieldset>
                                </form>
                            </div>';
        print '</div>';

                    // Add members
                    print '<br><br>
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="page-header"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Team members</h1>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8 col-md-push-2">
                                <br><br>
                                <p class="text-center">Add new member</p>
                                <form id="form_add_member" name="form_add_member" role="form">
                                    <fieldset>
                                        <div id="alert_member_info"></div>
                                        <div class="form-group">
                                            <input id="input_member_group_id" type="hidden" name="group_id"';

                                                if(!empty($project))
                                                    print 'value="' . $project->get_group_id() .'"';
                                            print '>
                                            <input id="input_member_project_id" type="hidden" name="project_id"';
                                                if(!empty($project))
                                                    print 'value="' . $project->get_id().'"';
                                            print '>

                                            <input id="input_member_group_professor_id" type="hidden" name="group_professor_id"'; 
                                                if(!empty($project))
                                                    print 'value="' . $project->get_group_professor_id().'"';
                                            print '>
                                            <input id="input_member_student_id" type="hidden" name="student_id"';
                                                if(!empty($project))
                                                    print 'value="' . $project->get_student_id().'"';
                                            print '>
                                            <input id="input_email" class="form-control" placeholder="Email" name="email" type="email" autofocus>
                                        </div>       
                                        <!-- Change this to a button or input when using this as a form -->
                                        <button id="add_member_btn" type="submit" class="btn btn-lg btn-primary btn-block">Add</button>
                                    </fieldset>
                                </form>
                            </div>
                        </div>';

                        print '<div id="other_members"></div>';
                }
                else
                {
                    print '<br><div class="alert alert-danger">This project doesn\'t exist</div></div>';
                }
            ?>
            </div>
        <br><br><br>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Quill -->
    <script src="js/custom/edit_project.js"></script>
</body>
</html>