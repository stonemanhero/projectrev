<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassStudent.php';
    include_once 'php/model/ClassProfessor.php';

    session_start();

    if(empty($_SESSION['user'])) 
    {
        header('Location: login.php');
        exit();
    }
    else 
    {
        header('Location: ' . $_SESSION['type'] . '.php');
        exit();
    }