<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassStudent.php';
    include_once 'php/model/ClassProfessor.php';
    include_once 'php/model/ClassGroup.php';
    include_once 'php/model/ClassProject.php';
    include_once 'php/model/ClassGroupHasStudent.php';
    include_once 'php/model/ClassProjectHasStudent.php';

    session_start();

    if(empty($_SESSION['user']))
    {
        header("Location: login.php");
        exit();
    }
    else
    {
        $user = $_SESSION['user'];
        //$type = $_SESSION['type'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ProjectRev - Project</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/fashion.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Main menu -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ProjectRev v1.0</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="groups.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Find group</a></li>
                    <?php
                    if($_SESSION['type'] == "professor")
                        print '<li><a href="create-group.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create group</a></li>';
                    else
                        print '<li><a href="create-project.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create project</a></li>';
                    ?>

                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-push-8">
                <p>Welcome, <?php if(!empty($_SESSION['user'])) print $_SESSION['user']->get_name(); ?> (<a href="php/logic/logout.php">logout</a>)</p>
            </div>
        </div>
        <div class="row">
            <?php
                if(isset($_GET['id']))
                {
                    $project = Project::select($_GET['id']);

                    if(!empty($project))
                    {
                        print '<div class="col-md-12">';
                            print '<h1 class="page-header"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> ' . $project->get_name()
                                . ' <button data-toggle="modal" data-target="#view_members" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View members</button> 
                                    <a target="_blank" href="' . $project->get_link() . '"><button class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View files</button></a>';
                            print '<br><span class="author">Created by ' . $project->get_student_name() . '</span>';
                        print '</div>';

                        print '<div class="col-md-12">';
                            print '<br>' . $project->get_description();
                        print '</div>';
                    }
                    else
                    {
                        print '<div class="alert alert-danger">This project doesn\'t exist</div>';
                    }
                }
                else
                {
                    print '<div class="alert alert-danger">This project doesn\'t exist</div>';
                }
            ?>
        </div>


        <?php
            if(!empty($project))
            {
                print '<div class="row">
                    <div class="col-md-12">
                        <br><br>
                        <h1 class="page-header"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Comments
                    </div>

                    <div class="col-md-6">
                        <br>
                        <form id="form_login" name="form_login" role="form">
                            <fieldset>
                                <input type="hidden" id="input_project_id" name="project_id" value="' . $project->get_id() . '">
                                <input type="hidden" id="input_name" name="name" value="' . $_SESSION['user']->get_name() . '">
                                <textarea class="form-control" name="message" id="input_message" placeholder="Comment" rows="3"></textarea>
                                <br>
                                <div id="alert_info">
                                </div>
                                <button id="add_comment" type="button" class="btn btn-lg btn-primary btn-block">Add comment</button>
                                <br>
                            </fieldset>
                        </form>
                    </div>


                    <div id="comments" class="col-md-8">
                    </div>
                </div>';
            }
        ?>
    </div>


    <!-- Modal View members -->
    <div class="modal fade" id="view_members">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title">Team members</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            
            <?php
                if(!empty($project))
                {
                    print '<p>Leader: <b>' . $project->get_student_name() . '</b></p>'; 
                
                    // Manage members
                    $members = ProjectHasStudent::get_members($project->get_id());

                    print '<div class="row">
                                <br><br>
                                <div class="col-md-8 col-md-push-2">
                                    <p class="text-center">Other members</p>';
                    if(!empty($members)) {
                        print ' <div id="table_mygroups">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table borderless">
                                                <thead>
                                                    <tr>
                                                        <th><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Student</th>
                                                        <th><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> ID</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';

                        foreach ($members as $key => $value) {
                            print '<tr>';
                                print '<td>' . $value->get_student_name() . '</td>';
                                print '<td>' . $value->get_student_index_number() . '</td>';
                            print '</tr>';
                        }

                        print '</tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                    }
                    else {
                        print '<br><div class="alert alert-warning">No other members on this project.</div>';
                    }

                    print '</div>';
                }
            ?>
          </div>

        </div>
      </div>
    </div>

    <br><br>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script src="js/custom/project.js"></script>
</body>
</html>