<?php
    include_once 'php/database/db.php';
    include_once 'php/model/ClassProfessor.php';
    include_once 'php/model/ClassGroup.php';
    include_once 'php/model/ClassGroupHasStudent.php';
    include_once 'php/model/ClassProject.php';

    session_start();

    if(empty($_SESSION['user'])) 
    {
        header('Location: login.php');
        exit();
    }
    else 
    {
        if($_SESSION['type'] === 'student') 
        {
            header('Location: student.php');
            exit();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ProjectRev - Overview</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/fashion.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Main menu -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ProjectRev v1.0</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="groups.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Find group</a></li>
                    <li><a href="create-group.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create group</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-push-8">
                <p>Welcome, <?php if(!empty($_SESSION['user'])) print $_SESSION['user']->get_name(); ?> (<a href="php/logic/logout.php">logout</a>)</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <h1 class="page-header"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> My groups</h1>
            </div>
        </div>

        <?php
            /* My groups */
            $groups = Group::select_by_professor($_SESSION['user']->get_id());

            if(!empty($groups))
            {
                print '<div class="row">
                                <div>
                                    <p class="pagealerts text-center"></p>
                                </div>
                                <div id="table_mygroups">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table borderless">
                                                <thead>
                                                    <tr>
                                                        <th><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> Group</th>
                                                        <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Creation date</th>
                                                        <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';

                foreach ($groups as $key => $value)
                {
                    print '<tr>';
                    print '<td>' . $value->get_name() . '</td>';
                    print '<td>' . $value->get_creation_date() . '</td>';
                    print '<td>
                                            <a href="group.php?id=' . $value->get_id() . '">
                                                <button class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</button>
                                            </a>
                                            <a href="edit-group.php?id=' . $value->get_id() . '">
                                                <button class="btn btn-success"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button>
                                            </a>
                                        </td>';
                    print '</tr>';
                }

                print '                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
            }
            else
            {
                print '<br><div class="alert alert-info">No groups created!</div>';
            }
        ?>

        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Group access requests</h1>
            </div>
        </div>

        <?php
            /* Group requests */
            $group_requests = GroupHasStudent::select_by_professor($_SESSION['user']->get_id());

            if(!empty($group_requests))
            {
                print '<div class="row">
                                    <div id="table_mygroups">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table borderless">
                                                    <thead>
                                                        <tr>
                                                            <th><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Student</th>
                                                            <th><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> ID</th>
                                                            <th><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Group</th>
                                                            <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>';
                foreach ($group_requests as $key => $value)
                {
                    print '<tr>';
                    print '<td>' . $value->get_student_name() . '</td>';
                    print '<td>' . $value->get_student_index() . '</td>';
                    print '<td>' . $value->get_group_name() . '</td>';
                    print '<td>
                                            <button 
                                                data-group-id="' . $value->get_group_id() . '"
                                                data-student-id="' . $value->get_student_id() . '"
                                                data-status="1"
                                                class="btn btn-primary process_group_request"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Allow</button>
                                            <button 
                                                data-group-id="' . $value->get_group_id() . '"
                                                data-student-id="' . $value->get_student_id() . '"
                                                data-status="-1"
                                                class="btn btn-danger process_group_request"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> Deny</button>
                                            </td>';
                    print '</td>';
                }

                print '                           </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>';

            }
            else
            {
                print '<br><div class="alert alert-info">No group requests!</div>';
            }
        ?>

        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Project requests</h1>
            </div>
        </div>

        <?php
            $projects = Project::get_requests($_SESSION['user']->get_id());

            if(!empty($projects))
            {
                print '<div class="row">
                                <div id="table_mygroups">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table borderless">
                                                <thead>
                                                    <tr>
                                                        <th><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> Name</th>
                                                        <th><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Leader</th>
                                                        <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Creation date</th>
                                                        <th><span class="glyphicon glyphicon-screenshot" aria-hidden="true"></span> Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';

                foreach ($projects as $key => $value)
                {
                    print '<tr>';
                    print '<td>' . $value->get_name() . '</td>';
                    print '<td>' . $value->get_student_name() . '</td>';
                    print '<td>' . $value->get_creation_date() . '</td>';
                    print '<td>
                                            <button 
                                                data-project-id="' . $value->get_id() . '"
                                                data-status="1"
                                                class="btn btn-primary process_project_request"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Allow</button>
                                            <button 
                                                data-project-id="' . $value->get_id() . '"
                                                data-status="-1"
                                                class="btn btn-danger process_project_request"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> Deny</button>
                                      </td>';
                    print '</tr>';
                }

                print '                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
            }
            else
            {
                print '<br><div class="alert alert-info">No project requests!</div>';
            }
        ?>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script src="js/custom/index.js"></script>
</body>
</html>